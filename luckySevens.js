function minimumBet(){
    var initialBet = document.getElementById("bet");
    if(initialBet.value == 0){
        alert("Your bet must bet greater than 0");
    }
    else{
        luckySevensGame();
    }
}

function rollTheDice() {
    return Math.floor((Math.random()*(7-1)))+1 + Math.floor((Math.random()*(7-1)))+1;

}

function luckySevensGame() {
    var initialBet= document.getElementById("bet").value;
    var initialBetAgain = initialBet;
    var largestMoneySum= initialBet;
    var numberOfRolls=0;
    var diceRollatLMS=0;
    while(initialBet>=1){
        var p = rollTheDice();
        if(p==7) {
            initialBet= initialBet+4;
            if(initialBet>largestMoneySum){
                largestMoneySum=initialBet;
                diceRollatLMS=numberOfRolls+1;
            }
            numberOfRolls +=1;
        }
        else {
            initialBet-=1;
            numberOfRolls +=1;
        }
        
    }
    insertTheResults(initialBetAgain,numberOfRolls,largestMoneySum, diceRollatLMS);
}

function insertTheResults(first, second, third, fourth){
    document.getElementById("firstRow").innerHTML = "<td>"+first+"</td>";
    document.getElementById("secondRow").innerHTML = "<td>"+second+"</td>";
    document.getElementById("thirdRow").innerHTML = "<td>"+third+"</td>";
    document.getElementById("fourthRow").innerHTML= "<td>"+fourth+"</td>";
    document.getElementById("theTable").style.display="block";
}